#include "PerformanceProfiler.h"

PerformanceProfiler::PerformanceProfiler():items(0),varianceOld(0),varianceNew(0),meanOld(0),meanNew(0){}
void PerformanceProfiler::update(double newVal)
{
  items++;

  //update mean
  if (items == 1)
  {
      meanOld = meanNew = newVal;
      varianceOld= 0.0;
  }
  else
  {
      meanNew = meanOld + (newVal - meanOld)/items;
      varianceNew = varianceOld + (newVal - meanOld)*(newVal - meanNew);

      // set up for next iteration
      meanOld=meanNew;
      varianceOld=varianceNew;
  }
}

double PerformanceProfiler::getMean()
{
  return meanNew;
}
double PerformanceProfiler::getVariance()
{
    return ( (items > 1) ? varianceNew/(items - 1) : 0.0 );
}
double PerformanceProfiler::getItems()
{
    return items;
}

std::string PerformanceProfiler::toString()
{
  std::string str=(std::to_string(items)+","+std::to_string(meanNew)+","+std::to_string((items > 1) ? varianceNew/(items - 1) : 0.0)+"\n");
  return str;
}
