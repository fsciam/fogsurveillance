#ifndef SMART_SURVEILLANCE_TYPES_
#define SMART_SURVEILLANCE_TYPES_

#include <opencv2/opencv.hpp>

enum{ FRAME_SENT=11, FRAME_RECEIVED, ARRAY_LENGHT,OBJ_SENT,OBJ_RECEIVED, READY, STOP_EXECUTION, CLOSE};

struct tracks_info_t
{
  int id;
  cv::Rect rect;
};

struct classifier_info_t
{
  int class_id;
  int track_id;
};
struct sys_performance_t
{
  int processed_frames;
  float avg_fps;
};
#endif //SMART_SURVEILLANCE_TYPES_
