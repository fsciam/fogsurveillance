#ifndef PERFORMACE_PROFILER_H
#define PERFORMACE_PROFILER_H
#include <string>
class PerformanceProfiler
{
  public:
    PerformanceProfiler();
    double getMean();
    double getVariance();
    double getItems();
    void update(double newVal);
    std::string toString();
  private:
    double meanOld;
    double meanNew;

    double varianceOld;
    double varianceNew;

    int items;
};
#endif //PERFORMACE_PROFILER_H
