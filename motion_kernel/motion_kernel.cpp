#include "dev/mango_hn.h"
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/bgsegm.hpp>
#include "smart_surveillance_types.h"
#include "PerformanceProfiler.h"

extern "C"
#pragma mango_kernel
void motion_kernel(int min_area,char * video_stream ,void * image_out, void * obj_out, int * num_objs, mango_event_t vs_e, mango_event_t img_out_e , mango_event_t obj_e,mango_event_t nob_e, mango_event_t next_e, void * kernel_performance)
{
  mango_wait(&vs_e, WRITE);
  std::cout << "KMOTION START\t opening video:\t"<< video_stream << '\n';
  cv::VideoCapture video_in(video_stream);

  PerformanceProfiler kernel_profiler;
  double clock_period= 1000/cv::getTickFrequency();

  if(!video_in.isOpened())
	{
		std::cout<<"Impossible to open video: "<<video_stream<<"\n";
    std::cout << "KMOTION VIDEO ENDED" << '\n';
    mango_write_synchronization(&next_e,STOP_EXECUTION);
    memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));

    mango_wait(&img_out_e,READY);
    mango_write_synchronization(&img_out_e,FRAME_SENT);
  	return;
	}
  cv::Mat frame,frame_resized,frame_smoothed,mask;

  float fps=video_in.get(cv::CAP_PROP_FPS);
  cv::Ptr<cv::BackgroundSubtractor> p_back_sub=cv::bgsegm::createBackgroundSubtractorCNT(fps*1,true,fps*60);

  bool next_stopped=false;

  for(int i=0;i<fps;i++)
  {
    if(video_in.read(frame))
  	{
      cv::resize(frame, frame_resized, cv::Size(416,416));
  		cv::bilateralFilter(frame_resized,frame_smoothed,9,200,200,cv::BORDER_DEFAULT);
  		p_back_sub->apply(frame_smoothed, mask);
    }
  }

  while( !next_stopped )
  {

    if(!video_in.read(frame))
    {
      std::cout << "KMOTION VIDEO ENDED" << '\n';
      mango_write_synchronization(&next_e,STOP_EXECUTION);

      mango_wait(&img_out_e,READY);
      mango_write_synchronization(&img_out_e,FRAME_SENT);

      memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));

      return;
    }

    double kernel_start_time=(double) cv::getTickCount();

    cv::resize(frame, frame_resized, cv::Size(416,416));
		cv::bilateralFilter(frame_resized,frame_smoothed,9,200,200,cv::BORDER_DEFAULT);
		p_back_sub->apply(frame_smoothed, mask);


    cv::erode(mask,mask,cv::Mat());
		cv::morphologyEx(mask,mask,cv::MORPH_CLOSE,cv::Mat());
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;

    cv::findContours(mask, contours,hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE );


		//build rectangles around moving objects
		std::vector<std::vector<cv::Point> > contours_poly(contours.size());
    std::vector<cv::Rect> filtered_rects;
    for( size_t i = 0; i < contours.size(); i++ )
    {

        cv::approxPolyDP( contours[i], contours_poly[i], 7, true );
        cv::Rect  bb=boundingRect( contours_poly[i] );
        if(bb.area()>min_area)
          filtered_rects.push_back(bb);
    }
    double kernel_end_time=(double) cv::getTickCount();
    kernel_profiler.update((kernel_end_time-kernel_start_time)*clock_period);

    std::cout << "KMOTION SEND FRAME" << '\n';
    mango_wait(&img_out_e,READY);
    uint32_t state= mango_read_synchronization(&next_e);
    std::cout << "KMOTION READY\t"<< state<< '\n';
    if( state != STOP_EXECUTION)
    {
      memcpy(image_out,frame_resized.data,3*416*416);
      mango_write_synchronization(&img_out_e,FRAME_SENT);
      mango_wait(&img_out_e,FRAME_RECEIVED);

      *num_objs=filtered_rects.size();
      std::cout << "KMOTION ABOUT TO SEND "<<*num_objs<<" rectangles"<< '\n';
      mango_write_synchronization(&nob_e,ARRAY_LENGHT);

  		for( size_t i = 0; i < filtered_rects.size(); i++ )
  		{

          std::cout << "KMOTION rectangles sent\t"<<i+1<<"\tout of\t"<<*num_objs<< '\n';
          memcpy(obj_out,&filtered_rects[i],sizeof(cv::Rect));
          mango_write_synchronization(&obj_e,OBJ_SENT);
          mango_wait(&obj_e,OBJ_RECEIVED);

      }
    }
    else
    {
      next_stopped=true;
      memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
      std::cout << "KMOTION IS TERMINATING" << '\n';
    }

  }


}
