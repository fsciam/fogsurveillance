
#include <opencv2/dnn.hpp>
#include "dev/mango_hn.h"
#include <iostream>
#include "smart_surveillance_types.h"
#include "Result.h"
#include "Track.h"
#include "HungarianAlgorithm.h"
#include "PerformanceProfiler.h"
#include "NeuralNetwork.h"

extern "C"
#pragma mango_kernel
void classifierKernel(void * image_in,  void * tracks_info_in,int * num_rect,char * model_path, char * config_path, int minIOU, void * class_info_out, int * num_info,mango_event_t img_in_e,mango_event_t tracks_info_in_e,mango_event_t model_e,mango_event_t config_e,mango_event_t class_info_out_e,mango_event_t num_rect_e,mango_event_t num_info_e,mango_event_t prev_e, mango_event_t next_e, void * kernel_performance)
{


  float minMotion=minIOU/100.0;
  std::cout << "KCLASSIFIER START\t"<<minMotion<< '\n';
  mango_wait(&model_e,WRITE);
  mango_wait(&config_e,WRITE);

  PerformanceProfiler kernel_profiler;
  double clock_period= 1000/cv::getTickFrequency();



  auto net = NeuralNetworkFactory::createFromFiles(std::string(model_path),std::string(config_path));
  bool next_stopped=false;
  while(!next_stopped)
  {
    std::vector<TrackerInfo> tracks;
    mango_write_synchronization(&img_in_e,READY);
    std::cout << "KCLASSIFIER READY" << '\n';
    mango_wait(&img_in_e,FRAME_SENT);
    std::cout << "KCLASSIFIER IMAGE RECEIVED" << '\n';
    if(mango_read_synchronization(&prev_e)==STOP_EXECUTION)
    {
      std::cout << "KCLASSIFIER END" << '\n';
      memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
      return;
    }
    std::cout << "KCLASSIFIER SAVING IMAGE" << '\n';
    cv::Mat frame=(cv::Mat(416,416,CV_8UC3,(uchar *)image_in)).clone();
    mango_write_synchronization(&img_in_e,FRAME_RECEIVED);
    mango_wait(&num_rect_e,ARRAY_LENGHT);
    for(int i=0;i<*num_rect;i++)
    {
      mango_wait(&tracks_info_in_e,OBJ_SENT);
      std::cout << "KCLASSIFIER obj pointer\t"<<tracks_info_in<<"\t"<<i<< '\n';
      tracks.push_back(*(static_cast<TrackerInfo *> (tracks_info_in)));
      mango_write_synchronization(&tracks_info_in_e,OBJ_RECEIVED);
    }


    double kernel_start_time=(double) cv::getTickCount();
    std::vector<std::pair<int,cv::Rect>> detectedObjs= net->classify(frame);

    std::vector<cv::Rect> boundingRects;
    for(auto o : detectedObjs)
    {
      boundingRects.push_back(o.second);
    }
    Result result=makeBoundingBoxesPair(tracks, boundingRects,minIOU);

    std::vector<classifier_info_t> classifierInfos;
    for(auto pair : result.getPairs())
    {
      classifier_info_t c;
      c.track_id=tracks[pair.first].getId();
      c.class_id=detectedObjs[pair.second].first;
      classifierInfos.push_back(c);
    }



    double kernel_end_time=(double) cv::getTickCount();
    kernel_profiler.update((kernel_end_time-kernel_start_time)*clock_period);

    if(!classifierInfos.empty())
    {
      *num_info=classifierInfos.size();
      std::cout << "KCLASSIFIER ABOUT TO SEND\t"<<*num_info<<"\tdetection"<< '\n';
      mango_write_synchronization(&num_info_e,ARRAY_LENGHT);
      mango_wait(&class_info_out_e,READY);
      if(mango_read_synchronization(&next_e) != STOP_EXECUTION)
      {
        for(int i=0;i<*num_info;i++)
        {
          memcpy(class_info_out,&classifierInfos[i],sizeof(classifier_info_t));
          std::cout << "KCLASSIFIER object sent\t"<<i<<"\tout of\t"<<*num_info<< '\n';
          mango_write_synchronization(&class_info_out_e,OBJ_SENT);
          mango_wait(&class_info_out_e,OBJ_RECEIVED);
        }
      }
      else
      {
        memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
        next_stopped=true;

      }
    }
  }
  std::cout << "KCLASSIFIER ABOUT TO END\t"<<'\n';
  mango_write_synchronization(&prev_e,STOP_EXECUTION);
  std::cout << "KCLASSIFIER TEST\t"<<mango_read_synchronization(&prev_e)<< '\n';
  mango_write_synchronization(&prev_e,STOP_EXECUTION);
  std::cout << "KCLASSIFIER TEST\t"<<mango_read_synchronization(&prev_e)<< '\n';
  mango_write_synchronization(&prev_e,STOP_EXECUTION);
  mango_write_synchronization(&img_in_e,READY);



}
