#include "NeuralNetwork.h"
TensorflowNetwork::TensorflowNetwork(std::string weights, std::string config) {net=cv::dnn::readNetFromTensorflow(weights,config);}
std::vector<std::pair<int,cv::Rect>> TensorflowNetwork::classify(cv::Mat frame)
{

  cv::Mat blob;
  cv::dnn::blobFromImage(frame, blob, 1.0,cv::Size(300,300),cv::Scalar(127.5,127.5,127.5),true,false);
  net.setInput(blob);
  cv::Mat prob = net.forward();

  cv::Mat detectionMat(prob.size[2], prob.size[3], CV_32F, prob.ptr<float>());
  int width=frame.size().width, height=frame.size().height;
  std::vector<std::pair<int,cv::Rect>> detectedObjs;

  for(int i=0;i<100;i++)
  {
    const  float* Mi = detectionMat.ptr<float>(i);

    if(Mi[2]>0.6)
    {
      int x=Mi[3]*width,y=Mi[4]*height;

      detectedObjs.push_back(std::make_pair(Mi[1],cv::Rect(x,y,Mi[5]*300,Mi[6]*300)));

    }
  }
  return detectedObjs;

}

DarknetNetwork::DarknetNetwork(std::string weights, std::string config)
{
  net=cv::dnn::readNetFromDarknet(config,weights);
  std::vector<std::string> layers_names=net.getLayerNames();
	for(auto s:net.getUnconnectedOutLayers())
	{
		layers.push_back(layers_names[s-1]);
	}
}

std::vector<std::pair<int,cv::Rect>> DarknetNetwork::classify(cv::Mat frame)
{


  cv::Mat blob;
  cv::dnn::blobFromImage(frame, blob, 1.0/255, cv::Size(416,416),cv::Scalar(0,0,0),true,false);
  net.setInput(blob);

  std::vector<cv::Mat> prob;
  net.forward(prob,layers);

  std::vector<cv::Rect> boxes;
  std::vector<float> confidences;
  std::vector<int> ids;
  for(auto output:prob)
  {
    float* data = (float*)output.data;
    for (int j = 0; j < output.rows; ++j, data += output.cols)
    {
        cv::Mat scores = output.row(j).colRange(5, output.cols);
        cv::Point classIdPoint;
        double confidence;
        cv::minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);
        if (confidence > 0.6)
        {
            int centerX = (int)(data[0] * frame.cols);
            int centerY = (int)(data[1] * frame.rows);
            int width = (int)(data[2] * frame.cols);
            int height = (int)(data[3] * frame.rows);
            int left = centerX - width / 2;
            int top = centerY - height / 2;

            ids.push_back(classIdPoint.x);
            confidences.push_back((float)confidence);
            boxes.push_back(cv::Rect(left, top, width, height));
        }
    }
  }


  std::vector<int> boxToDraw;
  cv::dnn::NMSBoxes(boxes,confidences,0.5,0.4,boxToDraw);

  std::vector<std::pair<int,cv::Rect>> detectedObjs;
  for(auto i:boxToDraw)
  {
    detectedObjs.push_back(std::make_pair(ids[i],boxes[i]));
  }

  return detectedObjs;
}

std::shared_ptr<NeuralNetwork> NeuralNetworkFactory::createFromFiles(std::string weights, std::string config)
{
  std::string ext = weights.substr(weights.rfind('.') + 1);
  if(ext == "pb")
    return std::make_shared<TensorflowNetwork>(weights, config);
  else if(ext == "weights")
    return std::make_shared<DarknetNetwork>(weights, config);
}
