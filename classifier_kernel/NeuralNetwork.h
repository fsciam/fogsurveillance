#ifndef NEURAL_NETWORK_
#define NEURAL_NETWORK_


#include <opencv2/dnn.hpp>
#include <memory>
#include <string>

class NeuralNetwork
{
  public:
    virtual std::vector<std::pair<int,cv::Rect>> classify(cv::Mat frame) = 0;
  protected:
    cv::dnn::Net net;
};

class DarknetNetwork : public NeuralNetwork
{
  public:
    DarknetNetwork(std::string weights, std::string config);
    std::vector<std::pair<int,cv::Rect>> classify(cv::Mat frame);
  private:
    std::vector<std::string> layers;
};

class TensorflowNetwork : public NeuralNetwork
{
  public:
    TensorflowNetwork(std::string weights, std::string config);
    std::vector<std::pair<int,cv::Rect>> classify(cv::Mat frame);
};

class NeuralNetworkFactory
{
  public:
      static std::shared_ptr<NeuralNetwork> createFromFiles(std::string weights, std::string config);
};
#endif //NEURAL_NETWORK_
