#include <opencv2/opencv.hpp>
#include <fstream>
#include "dev/mango_hn.h"

#include "smart_surveillance_types.h"
#include "PerformanceProfiler.h"
extern "C"
#pragma mango_kernel
void gui_kernel(char * file_path,void * image_in,int * num_info, void * info_in,void * classifier_infos, int * classifier_infos_num, mango_event_t path_e,mango_event_t img_in_e, mango_event_t num_info_e, mango_event_t tracks_info_e,mango_event_t prev_e, void * kernel_performance, void * sys_performance, mango_event_t classifier_infos_e, mango_event_t classifier_infos_num_e,mango_event_t class_e)
{
  std::cout << "KGUI STARTING" << '\n';

  mango_wait(&path_e,WRITE);
  std::ifstream ifs(file_path);
	std::string line;
	std::vector<std::string> classes;
	while (std::getline(ifs, line))
	{
		classes.push_back(line);
	}

  cv::Scalar colors[20];

  cv::RNG rng;
  for(int i=0; i<20 ;i++)
  {
      colors[i]=cv::Scalar((int)rng.uniform(0,256),(int)rng.uniform(0,256),(int)rng.uniform(0,256));
  }


  cv::namedWindow("Out");
  cv::moveWindow("Out", 500,500);


  PerformanceProfiler kernel_profiler;
  PerformanceProfiler sys_profiler;
  double clock_period= 1000/cv::getTickFrequency();

  bool user_continue=true;
  std::map<int,std::string> class_ids;

  while(user_continue )
  {
    double loop_start_time=(double) cv::getTickCount();
    std::vector<tracks_info_t> tracks;

    mango_write_synchronization(&img_in_e,READY);
    std::cout << "KGUI READY WAITING FRAME" << '\n';
    mango_wait(&img_in_e,FRAME_SENT);
    std::cout << "KGUI READY TO ROCK AND ROLL" << '\n';
    if(mango_read_synchronization(&prev_e)==STOP_EXECUTION)
    {
      std::cout << "KGUI END" << '\n';
      memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
      memcpy(sys_performance,&sys_profiler,sizeof(PerformanceProfiler));
      mango_write_synchronization(&prev_e,STOP_EXECUTION);
      mango_write_synchronization(&img_in_e,READY);
      mango_write_synchronization(&class_e,STOP_EXECUTION);
      mango_write_synchronization(&classifier_infos_e,READY);
      return;
    }
    cv::Mat frame=(cv::Mat(416,416,CV_8UC3,(uchar *)image_in)).clone();
    std::cout << "KGUI FRAME RECEIVED" << '\n';
    mango_write_synchronization(&img_in_e,FRAME_RECEIVED);
    std::cout << "KGUI WAITING INFOS" << '\n';
    mango_wait(&num_info_e,ARRAY_LENGHT);
    std::cout << "KGUI WAITING\t"<<*num_info<<"\tresults" << '\n';
    for(int i=0;i<*num_info;i++)
    {
      mango_wait(&tracks_info_e,OBJ_SENT);
      tracks.push_back(*(static_cast<tracks_info_t *> (info_in)));
      mango_write_synchronization(&tracks_info_e,OBJ_RECEIVED);
    }

    if(mango_read_synchronization(&classifier_infos_num_e) == ARRAY_LENGHT)
    {
      mango_write_synchronization(&classifier_infos_e,READY);

      for(int i=0;i<*classifier_infos_num;i++)
      {
        mango_wait(&classifier_infos_e,OBJ_SENT);
        classifier_info_t id_pair=(*(static_cast<classifier_info_t *> (classifier_infos)));
        if(class_ids.find(id_pair.track_id) == class_ids.end())
        {
          class_ids.insert(std::pair<int,std::string>(id_pair.track_id,classes[id_pair.class_id]));
        }
        else
        {
          class_ids.at(id_pair.track_id)=classes[id_pair.class_id];
        }
        mango_write_synchronization(&classifier_infos_e,OBJ_RECEIVED);
      }
    }

    double kernel_start_time=(double) cv::getTickCount();

    for(auto t : tracks)
    {
      int track_id=t.id;
      cv::Rect boundingBox=t.rect;

      std::string label;
      if(class_ids.find(track_id) == class_ids.end())
      {
        class_ids.insert(std::pair<int,std::string>(track_id,"?"));
        label=std::to_string(track_id)+" ?";
      }
      else
      {
        label=std::to_string(track_id)+" "+class_ids.at(track_id);
      }

      cv::Scalar color=colors[t.id%20];

      cv::rectangle(frame,boundingBox,color,2);
      cv::putText(frame,label,cv::Point(boundingBox.x,boundingBox.y+15),cv::FONT_HERSHEY_SIMPLEX,1,cv::Scalar(0,0,255));

    }

    cv::imshow("Out",frame);
    if(cv::waitKey(1) == 27)
    {
      user_continue=false;
    }

    double kernel_end_time=(double) cv::getTickCount();
    kernel_profiler.update((kernel_end_time-kernel_start_time)*clock_period);
    sys_profiler.update((kernel_end_time-loop_start_time)*clock_period);

    
  }

  cv::destroyAllWindows();

  memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
  memcpy(sys_performance,&sys_profiler,sizeof(PerformanceProfiler));
  mango_write_synchronization(&prev_e,STOP_EXECUTION);
  mango_write_synchronization(&img_in_e,READY);
  mango_write_synchronization(&class_e,STOP_EXECUTION);
  mango_write_synchronization(&classifier_infos_e,READY);

  std::cout << "KGUI TERMINATED BY USER REQUEST" << '\n';

}
