#include "kernel_runner.h"

#include <opencv2/core.hpp>
#include <memory>
#include <iostream>
#include <fstream>

#include "Track.h"
#include "PerformanceProfiler.h"


KernelRunner::KernelRunner(int minArea,int minIOUTracker, int minIOUMotion,std::string videoPath, std::string modelPath,std::string configPath, std::string classesPath)
{
  //Init logger
  mango::mango_init_logger();
  //Initialize runtime library, recipe generic
  mango_rt = new mango::BBQContext("smart_surveillance", "surveillance");

  //create kernel functions
  auto kf_motion = new mango::KernelFunction();
  auto kf_classifier = new mango::KernelFunction();
  auto kf_tracker= new mango::KernelFunction();
  auto kf_gui= new mango::KernelFunction();

  //load kernel
  kf_motion->load("/opt/mango/usr/local/share/smart_surveillance/motion_kernel/motion_kernel", mango::UnitType::GN, mango::FileType::BINARY);
  kf_classifier->load("/opt/mango/usr/local/share/smart_surveillance/classifier_kernel/classifier_kernel", mango::UnitType::GN, mango::FileType::BINARY);
  kf_tracker->load("/opt/mango/usr/local/share/smart_surveillance/tracker_kernel/tracker_kernel", mango::UnitType::GN, mango::FileType::BINARY);
  kf_gui->load("/opt/mango/usr/local/share/smart_surveillance/gui_kernel/gui_kernel", mango::UnitType::GN, mango::FileType::BINARY);

  //registration of kernel functions into task graph
  auto k_motion  = mango_rt->register_kernel(KMOTION, kf_motion, {MOTION_VIDEO_IN}, {MOTION_RECT_OUT,MOTION_MAT_OUT,MOTION_NUM_RECT, MOTION_TASK});
  auto k_tracker  = mango_rt->register_kernel(KTRACKER, kf_tracker, {MOTION_RECT_OUT,MOTION_MAT_OUT,MOTION_NUM_RECT}, {TRACKER_MAT_OUT,TRACKER_INFO_OUT,TRACKER_NUM_TRACKS,TRACKER_CLASS_INFO,TRACKER_CLASS_INFO_NUM,TRACKER_CLASS_MAT,TRACKER_TASK});
  auto k_classifier  = mango_rt->register_kernel(KCLASSIFIER, kf_classifier, {TRACKER_INFO_OUT,TRACKER_NUM_TRACKS,TRACKER_CLASS_MAT,CLASSIFIER_MODEL_IN,CLASSIFIER_CONFIG_IN},
    {CLASSIFIER_DETECTION_OUT,CLASSIFIER_NUM_DETECTION,CLASSIFIER_TASK});
  auto k_gui = mango_rt->register_kernel(KGUI, kf_gui,{TRACKER_MAT_OUT,TRACKER_INFO_OUT,TRACKER_NUM_TRACKS,GUI_CLASSES_PATH,CLASSIFIER_DETECTION_OUT,CLASSIFIER_NUM_DETECTION},{GUI_SYS,GUI_TASK});

  //registration of Buffers


  //KMOTION IN
  auto b_motion_video_in=mango_rt->register_buffer(MOTION_VIDEO_IN,(videoPath.size()+1)*sizeof(char),{},{KMOTION});
  //KMOTION OUT
  auto b_motion_mat_out=mango_rt->register_buffer(MOTION_MAT_OUT,416*416*3*sizeof(uchar),{KMOTION},{KTRACKER});
  auto b_motion_rect_out=mango_rt->register_buffer(MOTION_RECT_OUT,sizeof(cv::Rect),{KMOTION},{KTRACKER});
  auto b_motion_num_rect=mango_rt->register_buffer(MOTION_NUM_RECT,sizeof(int),{KMOTION},{KTRACKER});
  auto b_motion_task=mango_rt->register_buffer(MOTION_TASK,sizeof(PerformanceProfiler),{KMOTION},{});

 //KCLASSIFIER IN
  auto b_classifier_model_in=mango_rt->register_buffer(CLASSIFIER_MODEL_IN,(modelPath.size()+1)*sizeof(char),{HOST},{KCLASSIFIER});
  auto b_classifier_config_in=mango_rt->register_buffer(CLASSIFIER_CONFIG_IN,(configPath.size()+1)*sizeof(char),{HOST},{KCLASSIFIER});
  //KCLASSIFIER OUT
  auto b_classifier_detection_out=mango_rt->register_buffer(CLASSIFIER_DETECTION_OUT,sizeof(classifier_info_t),{KCLASSIFIER},{KGUI});
  auto b_classifier_num_det=mango_rt->register_buffer(CLASSIFIER_NUM_DETECTION,sizeof(int),{KCLASSIFIER},{KGUI});
  auto b_classifier_task=mango_rt->register_buffer(CLASSIFIER_TASK,sizeof(PerformanceProfiler),{KCLASSIFIER},{});
  //KTRACKER
  auto b_tracker_mat_out=mango_rt->register_buffer(TRACKER_MAT_OUT,416*416*3*sizeof(uchar),{KTRACKER},{KGUI});
  auto b_tracker_info_out=mango_rt->register_buffer(TRACKER_INFO_OUT,sizeof(tracks_info_t),{KTRACKER},{KGUI});
  auto b_tracker_num_tracks=mango_rt->register_buffer(TRACKER_NUM_TRACKS,sizeof(int),{KTRACKER},{KGUI});
  auto b_tracker_mat_class=mango_rt->register_buffer(TRACKER_CLASS_MAT,416*416*3*sizeof(uchar),{KTRACKER},{KCLASSIFIER});
  auto b_tracker_class_out=mango_rt->register_buffer(TRACKER_CLASS_INFO,sizeof(TrackerInfo),{KTRACKER},{KCLASSIFIER});
  auto b_tracker_num_class=mango_rt->register_buffer(TRACKER_CLASS_INFO_NUM,sizeof(int),{KTRACKER},{KCLASSIFIER});
  auto b_tracker_task=mango_rt->register_buffer(TRACKER_TASK,sizeof(PerformanceProfiler),{KTRACKER},{});
  //KGUI
  auto b_gui_classes=mango_rt->register_buffer(GUI_CLASSES_PATH,(classesPath.size()+1)*sizeof(char),{HOST},{KGUI});
  //KGUI
  auto b_gui_task=mango_rt->register_buffer(GUI_TASK,sizeof(PerformanceProfiler),{KGUI},{});
  auto b_gui_sys=mango_rt->register_buffer(GUI_SYS,sizeof(PerformanceProfiler),{KGUI},{});

	//register events
	auto mot_tra_evnt=mango_rt->register_event({KMOTION,KTRACKER},{KTRACKER,KMOTION});
	auto tra_cla_evnt=mango_rt->register_event({KCLASSIFIER,KTRACKER},{KTRACKER,KCLASSIFIER});
	auto tra_gui_evnt=mango_rt->register_event({KTRACKER,KGUI},{KGUI,KTRACKER});
  auto cla_gui_evnt=mango_rt->register_event({KCLASSIFIER,KGUI},{KGUI,KCLASSIFIER});


  //create task graph and allocate resources
  auto taskGraph= new mango::TaskGraph({k_motion,k_classifier,k_tracker,k_gui},{b_motion_video_in,b_motion_mat_out,b_motion_rect_out,b_motion_num_rect,b_motion_task,
  b_classifier_model_in,b_classifier_config_in,b_classifier_detection_out, b_classifier_num_det,b_classifier_task,b_tracker_mat_out,b_tracker_info_out, b_tracker_num_tracks,b_tracker_mat_class,b_tracker_class_out,b_tracker_num_class,b_tracker_task,b_gui_classes, b_gui_sys,b_gui_task},
	{mot_tra_evnt,tra_cla_evnt,tra_gui_evnt, cla_gui_evnt});




  mango_rt->resource_allocation(*taskGraph);






  auto arg_m_video_in=new mango::BufferArg(b_motion_video_in);
  auto arg_m_mat_out=new mango::BufferArg(b_motion_mat_out);
  auto arg_m_rect_out=new mango::BufferArg(b_motion_rect_out);
  auto arg_m_num_rect=new mango::BufferArg(b_motion_num_rect);
  auto arg_m_task=new mango::BufferArg(b_motion_task);

  auto arg_c_model_in=new mango::BufferArg(b_classifier_model_in);
  auto arg_c_config_in=new mango::BufferArg(b_classifier_config_in);

  auto arg_c_detection_out=new mango::BufferArg(b_classifier_detection_out);
  auto arg_c_num_det=new mango::BufferArg(b_classifier_num_det);
  auto arg_c_task=new mango::BufferArg(b_classifier_task);

  auto arg_t_mat_out=new mango::BufferArg(b_tracker_mat_out);
  auto arg_t_info_out=new mango::BufferArg(b_tracker_info_out);
  auto arg_t_num_tracks=new mango::BufferArg(b_tracker_num_tracks);
  auto arg_t_mat_class=new mango::BufferArg(b_tracker_mat_class);
  auto arg_t_class_out=new mango::BufferArg(b_tracker_class_out);
  auto arg_t_num_class=new mango::BufferArg(b_tracker_num_class);
  auto arg_t_task=new mango::BufferArg(b_tracker_task);

  auto arg_g_classes=new mango::BufferArg(b_gui_classes);
  auto arg_g_task=new mango::BufferArg(b_gui_task);
  auto arg_g_sys=new mango::BufferArg(b_gui_sys);

  auto arg_m_video_in_evnt=new mango::EventArg(b_motion_video_in->get_event());
  auto arg_m_mat_out_evnt=new mango::EventArg(b_motion_mat_out->get_event());
  auto arg_m_rect_out_evnt=new mango::EventArg(b_motion_rect_out->get_event());
  auto arg_m_num_rect_evnt=new mango::EventArg(b_motion_num_rect->get_event());

  auto arg_c_model_in_evnt=new mango::EventArg(b_classifier_model_in->get_event());
  auto arg_c_config_in_evnt=new mango::EventArg(b_classifier_config_in->get_event());
  auto arg_c_detection_out_evnt=new mango::EventArg(b_classifier_detection_out->get_event());
  auto arg_c_num_det_evnt=new mango::EventArg(b_classifier_num_det->get_event());

  auto arg_t_mat_out_evnt=new mango::EventArg(b_tracker_mat_out->get_event());
  auto arg_t_info_out_evnt=new mango::EventArg(b_tracker_info_out->get_event());
  auto arg_t_num_tracks_evnt=new mango::EventArg(b_tracker_num_tracks->get_event());
  auto arg_t_class_out_evnt=new mango::EventArg(b_tracker_class_out->get_event());
  auto arg_t_num_class_evnt=new mango::EventArg(b_tracker_num_class->get_event());
  auto arg_t_mat_class_evnt=new mango::EventArg(b_tracker_mat_class->get_event());
  auto arg_g_classes_evnt=new mango::EventArg(b_gui_classes->get_event());

  auto arg_mot_tra_evnt=new mango::EventArg(mot_tra_evnt);
  auto arg_tra_cla_evnt=new mango::EventArg(tra_cla_evnt);
  auto arg_tra_gui_evnt=new mango::EventArg(tra_gui_evnt);
  auto arg_cla_gui_evnt=new mango::EventArg(cla_gui_evnt);



  auto arg_minIOU=new mango::ScalarArg<int>(minIOUTracker);
  auto arg_movThr=new mango::ScalarArg<int>(minIOUMotion);
  auto arg_minArea=new mango::ScalarArg<int>(minArea);

  b_motion_video_in->write(videoPath.c_str());
  b_classifier_model_in->write(modelPath.c_str());
  b_classifier_config_in->write(configPath.c_str());
  b_gui_classes->write(classesPath.c_str());
  argsKMOTION = new mango::KernelArguments({arg_minArea,arg_m_video_in,arg_m_mat_out,arg_m_rect_out,arg_m_num_rect,arg_m_video_in_evnt,arg_m_mat_out_evnt,arg_m_rect_out_evnt, arg_m_num_rect_evnt,arg_mot_tra_evnt,arg_m_task},k_motion);
  argsKCLASSIFIER = new mango::KernelArguments({arg_t_mat_class,arg_t_class_out,arg_t_num_class,arg_c_model_in,arg_c_config_in,arg_movThr,arg_c_detection_out,arg_c_num_det,arg_t_mat_class_evnt,arg_t_class_out_evnt,arg_c_model_in_evnt,arg_c_config_in_evnt,arg_c_detection_out_evnt,arg_t_num_class_evnt,arg_c_num_det_evnt,arg_tra_cla_evnt,arg_cla_gui_evnt,arg_c_task},k_classifier);
  argsKTRACKER = new mango::KernelArguments({arg_m_mat_out,arg_m_rect_out,arg_m_num_rect,arg_t_mat_out,arg_t_info_out,arg_t_num_tracks,arg_t_mat_class,arg_t_class_out,arg_t_num_class,arg_minIOU,arg_m_mat_out_evnt,arg_m_rect_out_evnt,arg_m_num_rect_evnt,arg_t_mat_out_evnt,arg_t_info_out_evnt,arg_t_num_tracks_evnt,arg_mot_tra_evnt,arg_tra_gui_evnt,arg_t_class_out_evnt,arg_t_num_class_evnt,arg_t_mat_class_evnt,arg_tra_cla_evnt,arg_t_task},k_tracker);
  argsKGUI = new mango::KernelArguments({arg_g_classes,arg_t_mat_out,arg_t_num_tracks,arg_t_info_out,arg_c_detection_out,arg_c_num_det,arg_g_classes_evnt,arg_t_mat_out_evnt,arg_t_num_tracks_evnt,arg_t_info_out_evnt,arg_tra_gui_evnt,arg_g_task,arg_g_sys,arg_c_detection_out_evnt,arg_c_num_det_evnt,arg_cla_gui_evnt},k_gui);

}

void KernelRunner::run_kernel()
{
  auto k_motion  = mango_rt->get_kernel(KMOTION);
  auto k_classifier   = mango_rt->get_kernel(KCLASSIFIER);
  auto k_tracker = mango_rt->get_kernel(KTRACKER);
  auto k_gui = mango_rt->get_kernel(KGUI);

  auto motion_buffer = mango_rt->get_buffer(MOTION_TASK);
  auto classifier_buffer = mango_rt->get_buffer(CLASSIFIER_TASK);
  auto tracker_buffer = mango_rt->get_buffer(TRACKER_TASK);
  auto gui_buffer = mango_rt->get_buffer(GUI_TASK);
  auto sys_buffer = mango_rt->get_buffer(GUI_SYS);

  PerformanceProfiler  motion_result, tracker_result,gui_result,classifier_result,sys_result;
  auto e1=mango_rt->start_kernel(k_motion, *argsKMOTION);
  auto e2=mango_rt->start_kernel(k_classifier, *argsKCLASSIFIER);
  auto e3=mango_rt->start_kernel(k_tracker, *argsKTRACKER);
  auto e4=mango_rt->start_kernel(k_gui,*argsKGUI);

  e4->wait();
  e3->wait();
  e2->wait();
  e1->wait();

  std::cout << "FINISHED" << '\n';
  motion_buffer->read(&motion_result);
  classifier_buffer->read(&classifier_result);
  tracker_buffer->read(&tracker_result);
  gui_buffer->read(&gui_result);
  sys_buffer->read(&sys_result);
  std::cout << "READ DONE" << '\n';
  std::ofstream motion_report,tracker_report,classifier_report,gui_report,sys_report;
  motion_report.open ("result/motion_report.csv",std::ios::out | std::ios::app);
  tracker_report.open ("result/tracker_report.csv",std::ios::out | std::ios::app);
  classifier_report.open ("result/classifier_report.csv",std::ios::out | std::ios::app);
  gui_report.open ("result/gui_report.csv",std::ios::out | std::ios::app);
  sys_report.open ("result/sys_report.csv",std::ios::out | std::ios::app);

  motion_report << motion_result.toString();
  tracker_report << tracker_result.toString();
  classifier_report << classifier_result.toString();
  gui_report << gui_result.toString();
  sys_report << sys_result.toString();

  motion_report.close();
  tracker_report.close();
  classifier_report.close();
  gui_report.close();
  sys_report.close();


}

KernelRunner::~KernelRunner()
{
    // Deallocation and teardown
    mango_rt->resource_deallocation(*tg);
}
