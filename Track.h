#ifndef TRACK_H
#define TRACK_H

#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>


class Track
{
  public:
    /**
    * Constructor
    */
    Track(cv::Rect rect);
    /**
    * Obj getter
    * @return obj tracked
    */
    cv::Rect getBoundingBox();
    /**
    * Obj getter
    * @return id of the object
    */
    int getId();
    /**
    * Return the cost of "pair" obj and o
    * @param o object
    * @return cost of the assignment
    */
    double computeCost(cv::Rect new_rect);
  protected:
    int id;
    cv::Rect boundingBox;
    cv::Point centroid;
};

/**
* Class that represents object tracker
*/
class Tracker : public Track
{

  public:

    Tracker(cv::Rect rect);
    /**
    * Update tracker based on the new detection
    * @param o new detection
    */
    void update(cv::Rect new_rect);
    /**
    * Update tracker after no new detection
    * @return true if the track is still valid
    */
    bool update();
    /**
    * Predict position using a Kalman filter
    * @param dT delta time
    */
    void predict(double dT);


  private:
    static int trackCount;
    int framesDropped;
    cv::KalmanFilter kf;

};
class TrackerInfo: public Track
{
  public:
    TrackerInfo(Tracker tracker);
};
#endif
