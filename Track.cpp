#include "Track.h"
#include <iostream>


int Tracker::trackCount=0;
Track::Track(cv::Rect rect):
boundingBox(rect)
{
  centroid=cv::Point(boundingBox.x+(boundingBox.width/2),boundingBox.y+(boundingBox.height/2));

  id=0;
}

cv::Rect Track::getBoundingBox()
{
  return boundingBox;
}
int Track::getId()
{
  return id;
}

double Track::computeCost(cv::Rect new_rect)
{
  double inter=(boundingBox & new_rect).area();
  double uni=boundingBox.area() + new_rect.area() - inter;
  return 1-inter/uni;
}



Tracker::Tracker(cv::Rect rect):
Track(rect)
{
  trackCount++;
  id=trackCount;
  framesDropped=0;

  int stateSize=4; //[x,y,x',y']
	int measSize=2; //[mx,my]
	int contrSize=0;

  kf=cv::KalmanFilter(stateSize,measSize,contrSize,CV_32F);
	cv::setIdentity(kf.transitionMatrix);
  /*
  [1,0,dT,0]
  [0,1,0,dT]

  dT will be set during prediction
  */
  kf.measurementMatrix = cv::Mat::zeros(measSize, stateSize, CV_32F);
  kf.measurementMatrix.at<float>(0) = 1.0f;
  kf.measurementMatrix.at<float>(5) = 1.0f;
  //process noise is set low because movement is relatively slow and smooth
  cv::setIdentity(kf.processNoiseCov, cv::Scalar::all(1e-5));
   // set measurement noise
  cv::setIdentity(kf.measurementNoiseCov, cv::Scalar::all(1e-4));
  // error covariance
  cv::setIdentity(kf.errorCovPost, cv::Scalar::all(1));

  //set initial state(center coordinates and velocity)
  cv::Mat state(stateSize, 1, CV_32F);

  state.at<float>(0) = centroid.x;
  state.at<float>(1) = centroid.y;
  state.at<float>(2) = 0;
  state.at<float>(3) = 0;
  kf.statePre=state;
  kf.statePost = state;

}

void Tracker::update(cv::Rect new_rect)
{
  boundingBox=new_rect;
  centroid=cv::Point(boundingBox.x+(boundingBox.width/2),boundingBox.y+(boundingBox.height/2));
  cv::Mat meas(2,1,CV_32F);
  meas.at<float>(0) = centroid.x;
  meas.at<float>(1) = centroid.y;
	kf.correct(meas);
  framesDropped=0;
}
bool Tracker::update()
{
  framesDropped++;
  return framesDropped<30;
}

void Tracker::predict(double dT)
{

    kf.transitionMatrix.at<float>(2) = dT;
    kf.transitionMatrix.at<float>(7) = dT;
		cv::Mat state = kf.predict();
    centroid=cv::Point(state.at<float>(0), state.at<float>(1));

}

TrackerInfo::TrackerInfo(Tracker tracker):Track(tracker.getBoundingBox())
{id=tracker.getId();}
