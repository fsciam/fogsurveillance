
#include "HungarianAlgorithm.h"
#include "Track.h"
#include "dev/mango_hn.h"
#include "smart_surveillance_types.h"
#include <opencv2/opencv.hpp>
#include <memory>
#include "PerformanceProfiler.h"

extern "C"
#pragma mango_kernel
void tracker_kernel(void * image_in, void * obj_in, int * num_obj_in, void * image_out_gui, void * tracks_out, int * num_tracks,void * img_out_class,void * class_info,int * class_info_num, int threshold,mango_event_t img_in_e,mango_event_t obj_e,mango_event_t nob_e,mango_event_t img_out_gui_e,mango_event_t tracks_out_e,mango_event_t tracks_num_e,mango_event_t prev_e, mango_event_t next_e,mango_event_t class_info_e,mango_event_t class_info_num_e, mango_event_t img_out_class_e,mango_event_t class_e, void * kernel_performance)
{

  std::vector<Tracker> tracks;
  float minIOU =threshold/100.0;
  std::cout << "KTRACKER START\t"<<minIOU<< '\n';

  PerformanceProfiler kernel_profiler;
  double clock_period= 1000/cv::getTickFrequency();

  double ticks=0;
  bool next_stopped=false;
  while(!next_stopped)
  {
    std::vector<cv::Rect> inputRects;
    mango_write_synchronization(&img_in_e,READY);
    std::cout << "KTRACKER READY" << '\n';
    mango_wait(&img_in_e,FRAME_SENT);
    if(mango_read_synchronization(&prev_e)==STOP_EXECUTION)
    {
        std::cout<< "KTRACKER ENDING" << '\n';
        mango_write_synchronization(&next_e,STOP_EXECUTION);
        std::cout << "KTRACKER WAITING KGUI" << '\n';
        mango_wait(&img_out_gui_e,READY);
        std::cout << "KTRACKER KGUI READY" << '\n';
        memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));

        mango_write_synchronization(&img_out_gui_e,FRAME_SENT);
        mango_write_synchronization(&class_e,STOP_EXECUTION);
        mango_wait(&img_out_class_e,READY);
        mango_write_synchronization(&img_out_class_e,FRAME_SENT);
        return;
    }
    cv::Mat frame=(cv::Mat(416,416,CV_8UC3,(uchar *)image_in)).clone();
    std::cout << "KTRACKER read image"<< '\n';
    mango_write_synchronization(&img_in_e,FRAME_RECEIVED);
    mango_wait(&nob_e,ARRAY_LENGHT);
    for(int i=0;i<*num_obj_in;i++)
    {
      std::cout << "KTRACKER wait obj\t"<<i<<"\tout of\t"<<*num_obj_in<<'\n';
      mango_wait(&obj_e,OBJ_SENT);
      cv::Rect * received=static_cast <cv::Rect *>(obj_in);
      inputRects.push_back(*received);
      std::cout << "KTRACKER received obj\t"<<i<< '\n';
      mango_write_synchronization(&obj_e,OBJ_RECEIVED);
    }

    double kernel_start_time=(double) cv::getTickCount();

    if(tracks.empty())
    {
      for(auto rects : inputRects)
      {
        tracks.push_back(Tracker (rects));
      }

    }
    else
    {
      double precTick = ticks;
      ticks = (double) cv::getTickCount();
      double dT = (ticks - precTick) / cv::getTickFrequency();

      for(int i=0;i<tracks.size();i++)
      {

        tracks[i].predict(dT);
      }
      if(!inputRects.empty())
      {
        //clean rects
        std::vector<std::vector<int>> overlap(inputRects.size());
        for(int i=0;i<tracks.size();i++)
        {
          for(int j=0;j<inputRects.size();j++)
          {
            if(tracks[i].computeCost(inputRects[j])<0.2)
              overlap[j].push_back(i);
          }
        }
        for(int i=overlap.size()-1;i>=0;i--)
        {
          if(overlap[i].size()>1)
          {

            inputRects.erase(inputRects.begin()+i);
            for(auto ix : overlap[i])
            {
              inputRects.push_back(tracks[ix].getBoundingBox());
            }
          }
        }
        Result result=makeBoundingBoxesPair(tracks, inputRects,minIOU);

        for(auto pair: result.getPairs())
        {

          tracks[pair.first].update(inputRects[pair.second]);
        }

        for(auto i: result.getUnPairedObjects())
        {
          tracks.push_back(Tracker(inputRects[i]));
        }
       for(int i=result.getUnPairedTracks().size()-1;i>=0;i--)
        {
          int idx=result.getUnPairedTracks()[i];
          if(!tracks[idx].update())
            tracks.erase(tracks.begin()+idx);
        }
      }
      else
      {
        for(int i=tracks.size()-1;i>=0;i--)
         {
           if(!tracks[i].update())
             tracks.erase(tracks.begin()+i);
         }
      }
    }
    double kernel_end_time=(double) cv::getTickCount();
    kernel_profiler.update((kernel_end_time-kernel_start_time)*clock_period);

    mango_wait(&img_out_gui_e,READY);
    if(mango_read_synchronization(&next_e) != STOP_EXECUTION)
    {
      memcpy(image_out_gui,frame.data,3*416*416);
      mango_write_synchronization(&img_out_gui_e,FRAME_SENT);
      std::cout << "KTRACKER FRAME SENT" << '\n';
      mango_wait(&img_out_gui_e,FRAME_RECEIVED);

      *num_tracks=tracks.size();
      mango_write_synchronization(&tracks_num_e,ARRAY_LENGHT);
      std::cout << "KTRACKER OBJECT LENGHT SENT" << '\n';
      for(int i=0; i<*num_tracks; i++)
      {
        tracks_info_t p;
        p.id=tracks[i].getId();
        p.rect=tracks[i].getBoundingBox();
        memcpy(tracks_out,&p,sizeof(tracks_info_t));
        mango_write_synchronization(&tracks_out_e,OBJ_SENT);
        mango_wait(&tracks_out_e,OBJ_RECEIVED);
      }
      if(tracks.size()  && mango_read_synchronization(&img_out_class_e) == READY)
      {
        if(mango_read_synchronization(&class_e)!= STOP_EXECUTION)
        {
          memcpy(img_out_class,frame.data,3*416*416);
          mango_write_synchronization(&img_out_class_e,FRAME_SENT);
          *class_info_num=tracks.size();
          mango_write_synchronization(&class_info_num_e,ARRAY_LENGHT);
          for(int i=0; i<*num_tracks; i++)
          {
            TrackerInfo t(tracks[i]);
            memcpy(class_info,&t,sizeof(TrackerInfo));
            mango_write_synchronization(&class_info_e,OBJ_SENT);
            mango_wait(&class_info_e,OBJ_RECEIVED);
          }
        }
        else
          {
              memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
              next_stopped=true;
          }
      }
    }
    else
    {
      std::cout << "KTRACKER ABOUT TO END" << '\n';
      memcpy(kernel_performance,&kernel_profiler,sizeof(PerformanceProfiler));
      next_stopped=true;
    }

  }


  mango_write_synchronization(&prev_e,STOP_EXECUTION);
  mango_write_synchronization(&img_in_e,READY);
  std::cout << "KTRACKER TERMINATED" << '\n';
}
